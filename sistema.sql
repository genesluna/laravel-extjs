CREATE DATABASE sistema
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;
USE sistema;

DROP TABLE IF EXISTS funcionarios;
CREATE TABLE funcionarios (
	id SMALLINT ( 5 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR ( 40 ) NOT NULL,
	data_nascimento DATE,	
	cpf CHAR ( 11 ) UNIQUE,
	sexo ENUM ( 'M', 'F' ),
	salario DECIMAL ( 10,2 ),
	beneficio SET ( 'VT', 'CM', 'CO' ),
	ativo ENUM ( '0', '1', '2' ) COMMENT '0=Demitido|1=Ativo|2=Afastado/Ferias'
) ENGINE = innodb CHARACTER SET utf8 COLLATE utf8_general_ci;