<?php

class FuncionarioController extends BaseController 
{
	protected $funcionarios;
	public function __construct(Funcionario $funcionarios)
	{
		$this->funcionarios = $funcionarios;
	}

	public function postCreate()
    {
		try {
			$funcionario = $this->funcionarios->fill(Input::json()->all());

			if ($funcionario->save()) {
				$success = true;
				$message = 'Salvo com sucesso!';
			} else {
				$success = false;
				$message = 'Erro';
				$tmp     = array();
				foreach (json_decode($funcionario->errors()) as $mens) {
					$tmp[] = $mens;
				}
				if(count($tmp) > 0) {
					if(isset($tmp[0])){
						$values  = array_values($tmp[0]);
						$message = implode(',', $values);
					}
				}
			}

			return Response::json(array('success' => $success, 'message' => $message), 200);
		} catch (Exception $e) {
			return Response::json(array('success' => false, 'message' => $e->getMessage()), 200);
		}
	}

	public function postRead()
    {
		try {
			$start  = Input::get('start', 0);
			$limit  = Input::get('limit', 50);
			$sort   = Input::get('sort', 'nome');
			$dir    = Input::get('dir', 'ASC');
			$filter = Input::get('filter');

			$funcionarios = $this->funcionarios->orderby($sort, $dir)->skip($start)->take($limit);

			if (is_array($filter)) {
				foreach($filter as $dados) {
					$campo      = $dados['field'];
					$valor      = $dados['data']['value'];
					$tipo       = $dados['data']['type'];
					$comparison = isset($dados['data']['comparison']) ? $dados['data']['comparison'] : '=';

					switch($tipo){
						case 'string' :
							$funcionarios->where($campo, 'LIKE', "%$valor%");
							Break;
						case 'list' :
							if (! strstr($valor, ',')) {
								$valor = '0,'.$valor;
							}
							$valor = explode(',', $valor);
							$funcionarios->whereIn($campo, $valor);
							Break;
						case 'boolean' :
							$valor = (bool) $valor;
							$funcionarios->where($campo, '=', $valor);
							Break;
						case 'numeric' :
							$funcionarios->where($campo, $comparison, $valor);
							Break;
						case 'date' :
							$funcionarios->where($campo, $comparison, $valor);
							Break;
					}
				}

				$array['data']  = $funcionarios->get()->toArray();

				$array['count'] = $funcionarios->count();
			} else {
				$array['data']  = $funcionarios->get()->toArray();
				$array['count'] = $this->funcionarios->all()->count();
			}

			return Response::json($array, 200);
		} catch (Exception $e) {
			return Response::json(array('success' => false, 'message' => utf8_encode( $e->getMessage())), 200);
		}
    }

	public function postUpdate()
    {
		try {
			$records = Input::json()->all();

			if($funcionarios = $this->funcionarios->find($records['id'])) {
				$funcionarios->fill($records);

				if ($funcionarios->save()){
					$success = true;
					$message = 'Salvo com sucesso!';
				} else {
					$success = false;
					$message = 'Erro';
					$tmp     = array();
					foreach (json_decode($funcionarios->errors()) as $mens) {
						$tmp[] = $mens;
					}
					if(count($tmp) > 0) {
						if(isset($tmp[0])){
							$values  = array_values($tmp[0]);
							$message = implode(',', $values);
						}
					}
				}
			} else {
				$success = false;
				$message = 'ID nao existe';
			}
			return Response::json(array('success' => $success, 'message' => $message), 200);
		} catch (Exception $e) {
			return Response::json(array('success' => false, 'message' => utf8_encode( $e->getMessage())), 200);
		}
	}

	public function postDestroy()
    {
		try {
			$records = Input::json()->all();
			$count   = 0;
			$success = false;
			if(is_array($records)){
				$ids     = array();
				foreach($records as $record) {
					$ids[] = $record['id'];
				}

				$count = count($ids);

				if ( $count > 0 ) {
					if($this->funcionarios->whereIn('id', $ids)->delete()){
						$success = true;
					} else {
						$success = false;
					}
				}
			}
			return Response::json(array('success' => $success, 'message' => "Total de registros deletados: $count"), 200);
		} catch (Exception $e) {
			return Response::json(array('success' => false, 'message' => utf8_encode( $e->getMessage())), 200);
		}
	}    
}