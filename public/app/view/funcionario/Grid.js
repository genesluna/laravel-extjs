
Ext.define('LaraExt.view.funcionario.Grid', {
 	extend       : 'Ext.grid.Panel'
 	,alias       : 'widget.funcionariogrid'
 	,autoDestroy : true
	,height      : 350
	,columnLines : true
	,forceFit    : true
	,cls         : 'custom-first-last'
	,viewConfig  : {
		loadMask                 : true
		,loadingText             : 'Carregando...'
		,emptyText               : 'Nenhum registro encontrado.'
		,preserveScrollOnRefresh : true
		,stripeRows              : true
		,enableTextSelection     : true
		,getRowClass             : function(record) { 
			if(!record.get('ativo')){
				return 'desativado';
			}
		}
	}

	,selModel : sm = Ext.create('Ext.selection.CheckboxModel', {
		checkOnly       : true
		,injectCheckbox : 1		
	})

	,plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
		clicksToEdit: 2
	})]

    ,features: [
		{
			ftype: 'summary',
			dock: 'bottom'
		}
		
		,{
			ftype           : 'filters'
			,encode         : false
			,menuFilterText : 'Filtros'
			,local          : false
			,filters: [
				{
					type       : 'string'
					,dataIndex : 'nome'
				}
				
				,{
					type        : 'date'
					,dataIndex  : 'data_nascimento'
					,dateFormat : 'Y-m-d'
					,beforeText : 'Antes'
					,afterText  : 'Depois'
					,onText     : 'Igual'
				}

				,{
					type       : 'string'
					,dataIndex : 'cpf'
				}

				,{
					type       : 'list'
					,dataIndex : 'sexo'
					,phpMode   : true
					,options   : [['M', 'Masculino'], ['F', 'Feminino']]
				}

				,{
					type       : 'numeric'
					,dataIndex : 'salario'
					,emptyText : 'Digite...'
				}				

				,{
					type       : 'list'
					,dataIndex : 'beneficio'
					,phpMode   : true
					,options   : [['VT', 'Vale'], ['CO', 'Odonto'], ['CM', 'Medico']]
				}

				,{
					type       : 'boolean'
					,dataIndex : 'ativo'
					,yesText   : 'Sim'
					,noText    : 'N&atilde;o'
				}
			]
		}		
	]

	,initComponent: function() {
		this.dockedItems = [{
			xtype     : 'toolbar'
			,dock     : 'top'
			,defaults : {
				xtype     : 'button'
				,disabled : true
			}

            ,items : [
				{
					itemId    : 'btnCreate'
					,text     : 'Adicionar'
					,tooltip  : 'Adicionar Registro'
					,iconCls  : 'icon-btn-create'
					,disabled : false
					,action   : 'create'
				}

				,{xtype : 'tbseparator'}

				,{
					itemId    : 'btnUpdate'
					,text     : 'Editar'
					,tooltip  : 'Editar Registro'
					,iconCls  : 'icon-btn-update'
					,action   : 'update'
				}

				,{xtype : 'tbseparator'}

				,{
					itemId    : 'btnDestroy'
					,text     : 'Excluir'
					,tooltip  : 'Excluir Registro'
					,iconCls  : 'icon-btn-destroy'
					,action   : 'destroy'
				}
			]
		}

		,{
			xtype        : 'pagingtoolbar'
			,dock        : 'bottom'
			,store       : this.getStore()
			,displayInfo : true
			,displayMsg  : 'Mostrando Registros {0} - {1} de {2}'
			,emptyMsg    : 'Nenhum registro encontrado.'
            ,items:[
				{xtype : 'tbseparator'}
				,{
					text      : 'Limpar Filtros'
					,tooltip  : 'Limpar Filtros'
					,iconCls  : 'icon-btn-clear'
					,action   : 'clear'
				}
			]
		}];

		this.callParent(arguments);
	}

    ,store : 'Funcionario'

    ,columns: [
		{
			xtype  : 'rownumberer'
			,width : 30
			,tdCls : 'custom'
		}

		,{
			text       : 'Nome'
			,flex      : 1
			,dataIndex : 'nome'
			,editor    : {
				xtype       : 'textfield'
				,allowBlank : false
			}
		}

		,{
			xtype      : 'datecolumn'
			,text      : 'Data Nascimento'
			,dataIndex : 'data_nascimento'
			,format    : 'd/m/Y'
			,editor    : {
				xtype       : 'datefield'
				,allowBlank : false
			}
		}

		,{
			text       : 'CPF'
			,dataIndex : 'cpf'
			,renderer  : function(v) {
				if(v != 0) {
					return Ext.util.Format.TextMask.setMask('999.999.999-99').mask(v); 
				}
			}	
			,editor    : {
				xtype    : 'textfield'
				,plugins : 'textmask'
                ,mask    : '999.999.999-99'
                ,vtype   : 'cpf'
			}
		}

		,{
			text       : 'Sexo'
			,dataIndex : 'sexo'
			,renderer  : function (val, meta, record) {
				if(val == 'M') {
					meta.tdCls = 'icon-btn-male';
					return 'Masculino';
				} else {
					meta.tdCls = 'icon-btn-female';
					return 'Feminino';
				}
			}
			,editor    : {
				xtype         : 'combobox'
				,allowBlank   : false
			    ,displayField : 'valor'
			    ,valueField   : 'id'
			    ,editable     : false
			    ,queryMode    : 'local'
			    ,store: {
			        fields: ['id', 'valor']
			        ,data: [
			            {id: 'M', valor: 'Masculino'}
			            ,{id: 'F', valor: 'Feminino'}
			        ]
			    }
			}
		}

		,{
			xtype             : 'numbercolumn'
			,text             : 'Salario'
			,width            : 120
			,dataIndex        : 'salario'
			,renderer         : 'brMoney'
			,summaryType      : 'sum'
			,totalSummaryType : 'sum'
 			,summaryRenderer  : function(v){
				v = Ext.num(v, 0);
				v = (Math.round((v - 0) * 100)) / 100;
				v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
				v = String(v);

				var ps = v.split('.');
				var whole = ps[0];
				var sub = ps[1] ? ','+ ps[1] : ',00';
				var r = /(\d+)(\d{3})/;

				while (r.test(whole)) {
					whole = whole.replace(r, '$1' + '.' + '$2');
				}

				v = whole + sub;

				if (v.charAt(0) == '-') {
					return '-R$' + v.substr(1);
				}

            	return '<b>Total: R$ ' + v + '</b>';
        	}
			,editor           : {
				xtype              : 'numberfield'
				,decimalSeparator  : ','
				,allowBlank        : false
				,allowNegative     : false
				,forceDecimals     : true
				,minValue          : 0
				,hideTrigger       : true
				,keyNavEnabled     : false
				,mouseWheelEnabled : false
			}
		}

		,{
			text       : 'Beneficios'
			,dataIndex : 'beneficio'
			,editor    : {
				xtype : 'funcionariocombo'
			}
		}

		,{
			xtype      : 'actioncolumn'
			,text      : 'Ativo?'
			,align     : 'center'
			,sortable  : false
			,hideable  : false
			,resizable : false
			,draggable : false
			,dataIndex : 'ativo'
			,width     : 80
			,items     : [
				{
					getClass: function(v, meta, rec) {
						return rec.get('ativo') == 1 ? 'icon-btn-enabled' : 'icon-btn-disabled';
					}
				}
			]
		}
	]
});
