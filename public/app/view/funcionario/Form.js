Ext.define('LaraExt.view.funcionario.Form', {
    extend         : 'Ext.form.Panel'
    ,alias         : 'widget.funcionarioform'
	,bodyPadding   : 10
	,border        : false
	,baseCls       : 'x-plain'
	,method        : 'POST'
	,frame         : true
	,reset         : true
	,waitMsgTarget : true
	,waitTitle     : 'Por favor aguarde...'
	,waitMsg       : 'Salvando'
	,defaultType   : 'textfield'

	,fieldDefaults : {
		msgTarget   : 'side'
		,labelStyle : 'font-weight:bold;'
		,labelWidth : 120
	}

	,defaults : {
		anchor           : '100%'
		,msgTarget       : 'side'
		,enableKeyEvents : true
		,selectOnFocus   : true
		,allowBlank      : false
	}

    ,initComponent: function() {
        this.items = [
			{
				fieldLabel : 'Nome'
				,name      : 'nome'
				,itemId    : 'nome'
            }

			,{
				xtype         : 'datefield'
				,fieldLabel   : 'Data Nascimento'
				,name         : 'data_nascimento'
				,submitFormat : 'Y-m-d'
			}

			,{
				fieldLabel : 'CPF'
				,name      : 'cpf'
				,plugins   : 'textmask'
                ,mask      : '999.999.999-99'
				,vtype     : 'cpf'
            }

			,{
				xtype          : 'combobox'
				,fieldLabel    : 'Sexo'
				,name          : 'sexo'
				,displayField  : 'valor'
				,valueField    : 'id'
				,editable      : false
				,selectOnFocus : false
				,queryMode     : 'local'
				,store         : {
					fields : ['id', 'valor']
					,data  : [
						{id: 'M', valor :	'Masculino'}
						,{id: 'F', valor :	'Feminino'}
					]
				}
			}

			,{
				xtype              : 'numberfield'
				,fieldLabel        : 'Salario'
				,name              : 'salario'
				,decimalSeparator  : ','
				,allowBlank        : false
				,allowNegative     : false
				,forceDecimals     : true
				,minValue          : 0
				,hideTrigger       : true
				,keyNavEnabled     : false
				,mouseWheelEnabled : false
			}

			,{
				xtype       : 'funcionariocombo'
				,fieldLabel : 'Beneficios'
				,name       : 'beneficio'
			}

			,{
				xtype           : 'checkboxfield'
				,fieldLabel     : 'Ativo?'
				,name           : 'ativo'
				,boxLabel       : 'Sim'
				,inputValue     : true
				,uncheckedValue : false
				,allowBlank     : true
            }
        ];

		this.callParent(arguments);
    }
});